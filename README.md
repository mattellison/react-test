## Overall Objective
Create an app that renders N number of blocks based on a given structure "key:value" delimited by a pipe character. Also include a delete button on each block that should remove it from the list.

See wireframe mockup for expected output [wireframe](https://gitlab.com/technicalinterview-doctorlogic/react-test/blob/master/React%20Exercise%20wireframe.jpg)

***
Notes:
- We understand there are several ways to solve this and do not expect the same way from every developer. 
- We are not trying to trick anyone. If you have any questions or need any clarifying information please dont hesistate to reach out. 
- This is a typescript application but you are completely free to use jsx files and this project should understand those just fine. Typescript would be an added bonus but not required.
- We are not intentionally trying to encourage or discourage the use of any patterns or conventions. Feel free to use whatever you believe is necessary to solve this problem.
- Extra points for adding any comments to explain a decision you made in the code and what information might you need to make you write it differently
***


## Examples
1. `x:test|y:fsdf|what:oh no`
1. `name:matt|name:john|name:michael`
1. `header1:stuff|header2:other stuff|header3:important things`


## Requirements

Functionality
- each block should flow horizontally to fill width of screen and overflow should fall to next row
- delete button within each block should remove that block from the page
  
Styling
-  blocks should flow in left to right pattern across the width of the screen 
-  these can be on multiple rows
   

### Optional
- Validation (what happens if users puts an extra set set of semi-colons)



This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
